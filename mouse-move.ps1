Add-Type -AssemblyName System.Windows.Forms

$tick = $true
$shakeSize = 1

while ($true)
{
  $Pos = [System.Windows.Forms.Cursor]::Position
  $x = If ($tick) {$pos.X + $shakeSize} Else {$pos.X - $shakeSize}
  $y = $pos.Y
  $tick = !$tick
  [System.Windows.Forms.Cursor]::Position = New-Object System.Drawing.Point($x, $y)
  Start-Sleep -Minutes 5
}