This is a simple powerscript script and startup command to launch the script to alternate left and right movements for your mouse.

To setup the startup command, add startup.cmd to C:\Users\<username>\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup and edit the path to the ps1 script.

Timing and movement preferences can be easily modifed in the powerscript file. 
